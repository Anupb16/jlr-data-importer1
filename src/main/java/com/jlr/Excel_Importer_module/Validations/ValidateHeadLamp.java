package com.jlr.Excel_Importer_module.Validations;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class ValidateHeadLamp implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");
		
		final List<String> categories = new ArrayList<>();
        categories.add(features.getJSONObject("172").getString("category"));
        categories.add(features.getJSONObject("173").getString("category"));
        categories.add(features.getJSONObject("174").getString("category"));
        categories.add(features.getJSONObject("175").getString("category"));
        categories.add(features.getJSONObject("176").getString("category"));
        categories.add(features.getJSONObject("177").getString("category"));
        categories.add(features.getJSONObject("178").getString("category"));
        long standardCount = categories.stream().filter(category -> category.equals("S")).count();

        if(standardCount > 1) {
            return new JSONObject().put("error", "More than 1 headlamp");
        }

        return new JSONObject().put("error", "");
	}

}
