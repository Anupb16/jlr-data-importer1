package com.jlr.Excel_Importer_module.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.annotations.Entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.JsonObject;

@Entity(ds = "jlr-db")
public class Cars extends Db {

	private String cid;
	private String price;
	private String market;
	private String manufacturer;
	private String model;
	private String trim;
	private String modelYear;
	private String fuel;
	private String modelCode;
	private String mid;
	private String mfid;
	private String moid;
	private String power;
	private String engineCapacity;
	private String fileName;
	private String bodyType;
	@JsonIgnore
	Map<String, Features> features;
	//private List<JsonObject> featureCodeJson;
	
	public Cars() {
		// TODO Auto-generated constructor stub
	}

	public Cars(String cid, String price, String market, String manufacturer, String model, String trim, String modelYear,
			String fuel, String modelCode, String mid, String mfid, String moid, String power, String engineCapacity,
			Map<String, Features> features, String fileName, String bodyType) {
		super();
		this.cid = cid;
		this.price = price;
		this.market = market;
		this.manufacturer = manufacturer;
		this.model = model;
		this.trim = trim;
		this.modelYear = modelYear;
		this.fuel = fuel;
		this.modelCode = modelCode;
		this.mid = mid;
		this.mfid = mfid;
		this.moid = moid;
		this.power = power;
		this.engineCapacity = engineCapacity;
		this.features = features;
		//this.featureCodeJson = featureCodeJson;
		this.fileName = fileName;
		this.bodyType = bodyType;
	}

	@Override
	public String toString() {
		return "Cars [cid=" + cid + ", price=" + price + ", market=" + market + ", manufacturer=" + manufacturer
				+ ", model=" + model + ", trim=" + trim + ", modelYear=" + modelYear + ", fuel=" + fuel + ", modelCode="
				+ modelCode + ", mid=" + mid + ", mfid=" + mfid + ", moid=" + moid + ", power=" + power + ", engineCapacity="
				+ engineCapacity + ", fileName=" + fileName + ", bodyType=" + bodyType + ", features=" + features + "]";
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getTrim() {
		return trim;
	}

	public void setTrim(String trim) {
		this.trim = trim;
	}

	public String getModelYear() {
		return modelYear;
	}

	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}

	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public Map<String, Features> getFeatures() {
		return features;
	}

	public void setFeatures(Map<String, Features> features) {
		this.features = features;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMfid() {
		return mfid;
	}

	public void setMfid(String mfid) {
		this.mfid = mfid;
	}
	
	public String getMoid() {
		return moid;
	}

	public void setMoid(String moid) {
		this.moid = moid;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(String engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}
	
	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	/*public List<JsonObject> getFeatureCodeJson() {
		return featureCodeJson;
	}

	public void setFeatureCodeJson(List<JsonObject> featureCodeJson) {
		this.featureCodeJson = featureCodeJson;
	}*/

	/*public void addFeature(Features feature) {
		
		if(feature.getFeatureName().equals("")) {
			feature.setFeatureName("Specifications");
		}
		
		if (this.features != null) {
			if (!this.features.containsKey(feature.getFeatureName()))
				this.features.put(feature.getFeatureName(), feature);
			else {
				Features featurePresent = this.features.get(feature.getFeatureName());
				List<SubFeatures> subFeatures = featurePresent.getSubFeatures();
				subFeatures.addAll(feature.getSubFeatures());
				feature.setSubFeatures(subFeatures);
				this.features.put(feature.getFeatureName(), feature);
			}
		} else {
			this.features = new HashMap<String, Features>();
			this.features.put(feature.getFeatureName(), feature);
		}
	}*/

//	public void addToFeatureList(Map<String, Features> features) {
//		this.featureList.addAll(features.values());
//		this.setFeatures(null);
//	}
}