package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlr.Excel_Importer_module.models.CarFiles;
import com.jlr.Excel_Importer_module.models.Cars;
import com.jlr.Excel_Importer_module.models.Features;
import com.jlr.Excel_Importer_module.models.Manufacturer;
import com.jlr.Excel_Importer_module.models.Market;
import com.jlr.Excel_Importer_module.models.SubFeatures;

/**
 * Excel importer Author: Nikhil Created Date: 24-05-2018
 */
public class App {

	List<String> files = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		
		// Credentials.init("159.65.157.96:10111", "root", "root", "jlr-db"); // Test
		// server
		// Credentials.init("159.89.165.35:10111", "root", "root", "jlr-db"); //
		// Production server
		// Credentials.init("159.89.165.35:10111", "root", "root", "jlr-db-sb"); // Test
		// server 2
		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb"); // Test server 3
		
		List<String> filePaths = new ArrayList<String>();

		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");

		App listFiles = new App();
		//AppValidator validator = new AppValidator();
		filePaths = listFiles.listAllFiles(folder);
		for (String file : filePaths) {
			System.out.println("");
			System.out.println("");
			System.out.println("file........" + file);
		//	validator.validateFiles(file);
			processFile(file);
		}
		System.out.println("DONE PROCESSING.........................");

		//System.out.println("************************************************************");

		// generateReport();

		//System.out.println("************************************************************");

		//System.out.println("Generating unique feature value............");
		// generateUniqueFeatureValue();
		//System.out.println("Generated unique feature value Successfully..........");
	}

	@SuppressWarnings("unchecked")
	private static void generateUniqueFeatureValue() {
		List<JSONObject> carModelsList = Db.searchAsJson(Query.select().from("CarModels"));

		Map<String, Set<String>> featureCodeName = new HashMap<String, Set<String>>();
		carModelsList.forEach(carModel -> {
			try {
				JSONArray featureListArray = carModel.getJSONArray("featureList");

				for (int i = 0; i < featureListArray.length(); i++) {

					JSONObject featureListObject = featureListArray.getJSONObject(i);

					JSONArray subFeaturesArray = featureListObject.getJSONArray("subFeatures");
					for (int j = 0; j < subFeaturesArray.length(); j++) {

						JSONObject subFeatureObject = subFeaturesArray.getJSONObject(j);

						final String key = subFeatureObject.getString("featureCode") + ","
								+ subFeatureObject.getString("subFeatureName").trim().replaceAll(",", "-");
						final String fileName = carModel.getString("fileName");
						if (!featureCodeName.containsKey(key)) {
							featureCodeName.put(key, new HashSet<>());
						}

						featureCodeName.get(key).add(fileName.substring(fileName.lastIndexOf("/") + 1));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		featureCodeName.forEach((key, value) -> {
			StringBuilder sb = new StringBuilder();
			value.forEach(fileName -> sb.append(fileName).append(" | "));
			System.out.println(key + "," + sb.toString());
		});
	}

	@SuppressWarnings("unchecked")
	private static void generateReport() {
		String regex = "[0-9, /, ,/.]+";
		List<JSONObject> carModelsList = Db.searchAsJson(Query.select().from("CarModels"));

		carModelsList.sort((a, b) -> {
			String aString = null;
			String bString = null;
			try {
				aString = a.getString("market") + "-" + a.getString("manufacturer") + "-" + a.getString("model") + "-"
						+ a.getString("modelYear") + "-" + a.getString("trim");

				bString = b.getString("market") + "-" + b.getString("manufacturer") + "-" + b.getString("model") + "-"
						+ b.getString("modelYear") + "-" + b.getString("trim");

			} catch (Exception e) {
				e.printStackTrace();
			}
			return aString.compareTo(bString);
		});

		System.out.println(
				"cid,market,manufacturer,model,modelCode,modelYear,bodyType,fuel,trim,segment,power,price,Price On road / drive away, Price before Tax,Price On road (RRP)");

		carModelsList.forEach(carModel -> {
			try {
				StringBuilder sb = new StringBuilder();
				sb.append(carModel.getString("cid"));
				sb.append(",");
				sb.append(carModel.getString("market"));
				sb.append(",");
				sb.append(carModel.getString("manufacturer"));
				sb.append(",");
				sb.append(carModel.getString("model"));
				sb.append(",");
				sb.append(carModel.getString("modelCode"));
				sb.append(",");
				sb.append(carModel.getString("modelYear"));
				sb.append(",");
				sb.append(carModel.getString("bodyType").replaceAll(",", " "));
				sb.append(",");
				sb.append(carModel.getString("fuel").replaceAll(",", " "));
				sb.append(",");
				sb.append(carModel.getString("trim").replaceAll(",", " "));
				sb.append(",");
				sb.append(carModel.getString("segment"));
				sb.append(",");
				sb.append(carModel.getString("power").replaceAll(",", " "));
				sb.append(",");
				if (carModel.getString("price") != "" || carModel.getString("price") != null) {

					if (carModel.getString("price").matches(regex)) {
						sb.append(carModel.getString("price").replaceAll(",", " "));
					} else {
						sb.append("");
					}
				}

				sb.append(",");

				JSONArray featureListArray = carModel.getJSONArray("featureList");

				String val1 = "";
				String val2 = "";
				String val3 = "";

				for (int i = 0; i < featureListArray.length(); i++) {
					JSONObject featureListObject = featureListArray.getJSONObject(i);
					String featureName = featureListObject.getString("featureName");
					if (featureName.equals("Specifications")) {

						JSONArray subFeaturesArray = featureListObject.getJSONArray("subFeatures");

						for (int j = 0; j < subFeaturesArray.length(); j++) {
							JSONObject subFeatureObject = subFeaturesArray.getJSONObject(j);

							if (subFeatureObject.getString("subFeatureName").equals("Price On road / drive away")) {
								val1 = subFeatureObject.getString("category").replaceAll(",", " ");
							}

							if (subFeatureObject.getString("subFeatureName").equals("Price before Tax")) {
								val2 = subFeatureObject.getString("category").replaceAll(",", " ");
							}

							if (subFeatureObject.getString("subFeatureName").equals("Price On road (RRP)")) {
								val3 = subFeatureObject.getString("category").replaceAll(",", " ");
							}
						}
					}
				}
				sb.append(val1);
				sb.append(",");
				sb.append(val2);
				sb.append(",");
				sb.append(val3);

				System.out.println(sb.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	public List<String> listAllFiles(File folder) {

		File[] fileNames = folder.listFiles();
		for (File file : fileNames) {
			if (file.isDirectory()) {
				listAllFiles(file);
			} else {
				try {
					files.addAll(readContent(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return files;
	}

	public List<String> readContent(File file) throws IOException {
		List<String> files = new ArrayList<String>();

		if (file.getCanonicalPath().contains("Archive") || file.getCanonicalPath().contains("archive")) {
		} else if (file.getCanonicalPath().contains(".DS_Store")) {
		} else {
			files.add(file.getCanonicalPath());
		}
		return files;
	}

	@SuppressWarnings({ "unchecked" })
	public static void processFile(String filePath) throws Exception {

		Map<String, Features> featuresMap = new HashMap<String, Features>();
		Cars carObject = new Cars();
		Features features = new Features();
		CarFiles carFile = new CarFiles();
		List<SubFeatures> subFeatures = new ArrayList<SubFeatures>();
		List<Features> featuresList = new ArrayList<Features>();
		Set<String> featureNames = new HashSet<String>();
		String presentMarket = null;
		int i = 0;
		int featureGroupstart = 2;
		int featureNamestart = 5;

		int lastIndex = filePath.lastIndexOf("\\");

		carObject.setFileName(filePath.substring(lastIndex + 1));
		carFile.setFileName(filePath.substring(lastIndex + 1));

		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);

		int firstRow = datatypeSheet.getFirstRowNum();
		int lastRow = datatypeSheet.getLastRowNum();

		Row row1 = datatypeSheet.getRow(firstRow + 1);

		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			datatypeSheet = workbook.getSheetAt(1);

			firstRow = datatypeSheet.getFirstRowNum();
			lastRow = datatypeSheet.getLastRowNum();
			row1 = datatypeSheet.getRow(firstRow + 1);
			if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
				datatypeSheet = workbook.getSheetAt(2);

				firstRow = datatypeSheet.getFirstRowNum();
				lastRow = datatypeSheet.getLastRowNum();
				row1 = datatypeSheet.getRow(firstRow + 1);
			}
		}

		Row row0 = datatypeSheet.getRow(3);

		if (row0.getCell(1) != null) {
			if (row0.getCell(1).toString().trim().equalsIgnoreCase("S")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("O")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("(O)")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("P")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("NA")) {
				featureGroupstart = 1;
				featureNamestart = 4;
			}
		}

		if (row0.getCell(3).toString() != null) {
			if (row0.getCell(3).toString().trim().equalsIgnoreCase("Model - Identifier")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("S")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("O")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("(O)")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("P")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("NA")) {
				featureGroupstart = 3;
				featureNamestart = 6;
			}
		}

		if (row1.getCell(5) != null && i == 0) {
			if (row1.getCell(5).toString().trim() != ""
					&& !row1.getCell(5).toString().trim().equalsIgnoreCase("Market")) {
				i = 5;
			}

		}
		if (row1.getCell(6) != null && i == 0) {
			if (row1.getCell(6).toString().trim() != ""
					&& !row1.getCell(6).toString().trim().equalsIgnoreCase("Market")) {
				i = 6;
			}
		}
		if (row1.getCell(7) != null && i == 0) {
			if (row1.getCell(7).toString().trim() != ""
					&& !row1.getCell(7).toString().trim().equalsIgnoreCase("Market")) {
				i = 7;
			}
		}
		if (row1.getCell(8) != null && i == 0) {
			if (row1.getCell(8).toString().trim() != ""
					&& !row1.getCell(8).toString().trim().equalsIgnoreCase("Market")) {
				i = 8;
			}
		}
		if (row1.getCell(9) != null && i == 0) {
			if (row1.getCell(9).toString().trim() != ""
					&& !row1.getCell(9).toString().trim().equalsIgnoreCase("Market")) {
				i = 9;
			}
		}

		Market market = new Market();

		while (i < row1.getLastCellNum()) {

			List<JSONObject> DbMarkets = Db.searchAsJson(Query.select().from("carmarkets"));
			List<JSONObject> DbManufacturers = Db.searchAsJson(Query.select().from("carmanufacturers"));

			Map<String, String> markets = new HashMap<String, String>();
			Map<String, String> manufacturers = new HashMap<String, String>();
			Map<String, String> UniqMarketManufacturers = new HashMap<String, String>();

			for (JSONObject json : DbMarkets) {
				markets.put(json.getString("market"), json.getString("mid"));
			}

			for (JSONObject json : DbManufacturers) {
				manufacturers.put(json.getString("manufacturer"), json.getString("mfid"));
				UniqMarketManufacturers.put(json.getString("mid") + " " + json.getString("manufacturer"),
						json.getString("mfid"));
			}

			/*
			 * System.out.println(""); Set st1 = (Set) UniqMarketManufacturers.entrySet();
			 * Iterator it1 = st1.iterator(); while (it1.hasNext()) { Map.Entry entry1 =
			 * (Entry) it1.next(); System.out.println( "Key*******" + " " + entry1.getKey()
			 * + " : " + "Value*******" + " " + entry1.getValue()); }
			 */

			for (int rn = 1; rn < 11; rn++) {
				Row row = datatypeSheet.getRow(rn);

				if (row.getCell(featureNamestart) != null) {
					if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Market")) {
						String marketVal = "";

						if (row.getCell(i).toString().trim().equals("Netherland")) {
							marketVal = "Netherlands";
						} else {
							marketVal = row.getCell(i).toString().trim();
						}

						if (markets.containsKey(marketVal)) {
							presentMarket = marketVal;
							market.setMarket(marketVal);
							market.setMid(markets.get(marketVal));
							carObject.setMarket(market.getMarket());
							carObject.setMid(market.getMid());
						} else {
							if (!marketVal.equals("")) {

								presentMarket = marketVal;
								market.setMarket(marketVal);
								market.setMid(UUID.randomUUID().toString());
								carObject.setMarket(marketVal);
								carObject.setMid(market.getMid());
								markets.put(market.getMarket(), market.getMid());

								JSONObject json = new JSONObject(new ObjectMapper().writeValueAsString(market));
								JsonParser jsonParser = new JsonParser();
								JsonObject marketJson = (JsonObject) jsonParser.parse(json.toString());

								if (!market.getMarket().trim().equals(""))
									Db.insertJson("carmarkets", marketJson);
							}
						}
					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Manufacturer")) {

						Manufacturer manufacturer = new Manufacturer();

						if (manufacturers.containsKey(row.getCell(i).toString().trim())) {

							List<JSONObject> carModels = new ArrayList<JSONObject>();

							if (markets.get(presentMarket) != null) {
								carModels = Db.searchAsJson(Query.select().from("carmanufacturers").where(SearchParam
										.create("mid").eq(markets.get(presentMarket))
										.and(SearchParam.create("manufacturer").eq(row.getCell(i).toString().trim()))));
							}

							if (carModels.size() == 0 || carModels == null) {

								manufacturer.setManufacturer(row.getCell(i).toString().trim());
								manufacturer.setMfid(UUID.randomUUID().toString());
								manufacturer.setMid(market.getMid());
								carObject.setManufacturer(manufacturer.getManufacturer());
								carObject.setMfid(manufacturer.getMfid());
								carObject.setMid(market.getMid());
								manufacturers.put(manufacturer.getManufacturer(), manufacturer.getMfid());
								JSONObject json1 = new JSONObject(new ObjectMapper().writeValueAsString(manufacturer));
								JsonParser jsonParser1 = new JsonParser();
								JsonObject manufacturerJson = (JsonObject) jsonParser1.parse(json1.toString());
								if (!manufacturer.getManufacturer().trim().equals(""))
									System.out.println("");
									Db.insertJson("carmanufacturers", manufacturerJson);
							} else {
								manufacturer.setManufacturer(row.getCell(i).toString().trim());
								// manufacturer.setMfid(manufacturers.get(row.getCell(i).toString().trim()));
								manufacturer.setMfid(UniqMarketManufacturers
										.get(markets.get(presentMarket) + " " + row.getCell(i).toString().trim()));

								manufacturer.setMid(markets.get(presentMarket));
								carObject.setManufacturer(manufacturer.getManufacturer());
								carObject.setMfid(manufacturer.getMfid());
								carObject.setMid(manufacturer.getMid());
							}
						} else {
							if (!row.getCell(i).toString().trim().equals("")) {
								manufacturer.setManufacturer(row.getCell(i).toString().trim());
								manufacturer.setMfid(UUID.randomUUID().toString());
								manufacturer.setMid(market.getMid());
								carObject.setManufacturer(manufacturer.getManufacturer());
								carObject.setMfid(manufacturer.getMfid());
								carObject.setMid(market.getMid());
								manufacturers.put(manufacturer.getManufacturer(), manufacturer.getMfid());
								JSONObject json1 = new JSONObject(new ObjectMapper().writeValueAsString(manufacturer));
								JsonParser jsonParser1 = new JsonParser();
								JsonObject manufacturerJson = (JsonObject) jsonParser1.parse(json1.toString());
								if (!manufacturer.getManufacturer().trim().equals(""))
									System.out.println("");
									Db.insertJson("carmanufacturers", manufacturerJson);
							}

						}
					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model")) {
						if (row.getCell(i).toString().trim().equals("E-pace")
								|| row.getCell(i).toString().trim().equals("E-Pace")) {
							carObject.setModel("E-PACE");
						} else if (row.getCell(i).toString().trim().equals("I-Pace")) {
							carObject.setModel("I-PACE");
						} else if (row.getCell(i).toString().trim().equals("F-Pace")) {
							carObject.setModel("F-PACE");
						} else if (row.getCell(i).toString().trim().equals("5 series")) {
							carObject.setModel("5 Series");
						} else if (row.getCell(i).toString().trim().equals("S-Class")) {
							carObject.setModel("S Class");
						} else if (row.getCell(i).toString().trim().equals("Land Cruiser")) {
							carObject.setModel("Land Cruiser Prado");
						} else {
							carObject.setModel(row.getCell(i).toString().trim());
						}

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Version / trim")) {
						carObject.setTrim(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model Year")) {
						carObject.setModelYear(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Fuel Type")) {
						carObject.setFuel(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Price - RRP / MSRP")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("RRP/MSRP")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Price RRP / MSRP")
							|| row.getCell(featureNamestart).toString().trim()
									.equalsIgnoreCase("Price On road (RRP)")) {
						if (carObject.getPrice() == "" || carObject.getPrice() == null) {
							carObject.setPrice(row.getCell(i).toString().trim());
						}

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model code")) {
						carObject.setModelCode(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power PS HP / (KW)")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power PS (HP) / KW")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power")) {
						carObject.setPower(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Engine Capacity (cc)")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Engine Capacity")) {
						carObject.setEngineCapacity(row.getCell(i).toString().trim());
					}
				}
			}

			for (int rn = 11; rn < 31; rn++) {
				Row row = datatypeSheet.getRow(rn);
				features.setFeatureName("Specifications");
				featureNames.add(features.getFeatureName());
				SubFeatures subFeature = new SubFeatures();
				if (row != null) {

					if (row.getCell(1) == null) {
					} else {
						if (!row.getCell(1).toString().isEmpty()) {
							double d = Double.parseDouble(row.getCell(1).toString());
							int code = (int) d;
							subFeature.setFeatureCode(code);
						}
					}

					subFeature.setSubFeatureName(row.getCell(featureNamestart).toString());

					if (row.getCell(i) == null) {
						subFeature.setCategory("");
					} else {
						subFeature.setCategory(row.getCell(i).toString());
						if (row.getCell(featureNamestart).toString().equals("Body Type")) {
							carObject.setBodyType(row.getCell(i).toString().trim());
						}
					}

					if (row.getCell(i + 1) == null) {
						subFeature.setPrice("");
					} else
						subFeature.setPrice(row.getCell(i + 1).toString());

					if (row.getCell(i + 2) == null) {
						subFeature.setComment("");
					} else
						subFeature.setComment(row.getCell(i + 2).toString());
				}
//				if (!subFeature.getSubFeatureName().trim().equalsIgnoreCase("Curtain airbag side window")
//						&& !subFeature.getSubFeatureName().trim().equalsIgnoreCase("Driver Knee Airbag"))
//					subFeatures.add(subFeature);
			}
			features.setSubFeatures(subFeatures);
			featuresMap.put(features.getFeatureName(), features);

			int lastRowTocheck = datatypeSheet.getLastRowNum();
			if (lastRowTocheck > 579) {
				lastRowTocheck = 579; // changed from 580
			} else {
				lastRowTocheck = datatypeSheet.getLastRowNum();
			}

			for (int rn = 31; rn <= lastRowTocheck; rn++) {
				features = new Features();
				SubFeatures subFeature = new SubFeatures();
				subFeatures = new ArrayList<SubFeatures>();
				Row row = datatypeSheet.getRow(rn);
				if (row != null) {

					features.setFeatureName(row.getCell(featureGroupstart).toString());
					featureNames.add(features.getFeatureName());
					if (row.getCell(featureNamestart) != null) {

						if (row.getCell(1) == null) {
						} else {
							if (!row.getCell(1).toString().isEmpty()) {

								double d = Double.parseDouble(row.getCell(1).toString());
								int code = (int) d;
								subFeature.setFeatureCode(code);
							}
						}

						if (row.getCell(featureNamestart) == null) {
							subFeature.setSubFeatureName("");
						} else
							subFeature.setSubFeatureName(row.getCell(featureNamestart).toString());

						if (row.getCell(i) == null) {
							subFeature.setCategory("");
						} else
							subFeature.setCategory(row.getCell(i).toString());

						if (row.getCell(i + 1) == null) {
							subFeature.setPrice("");
						} else
							subFeature.setPrice(row.getCell(i + 1).toString());

						if (row.getCell(i + 2) == null) {
							subFeature.setComment("");
						} else
							subFeature.setComment(row.getCell(i + 2).toString());
					}
				}
				if (subFeature.getSubFeatureName() != "" && subFeature.getSubFeatureName() != null)
					subFeatures.add(subFeature);
				features.setSubFeatures(subFeatures);

				if (!features.getSubFeatures().isEmpty()) {
					if (featuresMap.containsKey(features.getFeatureName())) {
						Features featurePresent = featuresMap.get(features.getFeatureName());
						List<SubFeatures> subFeatures2 = featurePresent.getSubFeatures();
						subFeatures2.addAll(features.getSubFeatures());
						features.setSubFeatures(subFeatures2);
						features.set_id(UUID.randomUUID().toString());
						featuresMap.put(features.getFeatureName(), features);
					} else {
						features.set_id(UUID.randomUUID().toString());
						featuresMap.put(features.getFeatureName(), features);
					}
				}
			}

			for (String key : featuresMap.keySet()) {
				featuresList.add(featuresMap.get(key));
			}

			//carObject.setFeatureList(featuresList);
			carObject.setCid(UUID.randomUUID().toString());
			carFile.setCarId(carObject.getCid());

			JSONObject json = new JSONObject(carObject);

			List<JSONObject> list = Db.searchAsJson(Query.select("type").from("CarTypes")
					.where(SearchParam.create("model").eq(json.getString("model").trim()))); // json.get(i).getString("model")
			if (list.size() > 0) {
				json.put("segment", list.get(0).getString("type"));
			}

			JsonParser jsonParser = new JsonParser();
			JsonObject carsJson = (JsonObject) jsonParser.parse(json.toString());
			if (carObject.getMarket() != null && !carObject.getMarket().trim().equals(""))
				Db.insertJson("carmodels", carsJson);

		///	JSONObject fileJson = new JSONObject(carFile);
		//	JsonObject jsonToInsert = (JsonObject) jsonParser.parse(fileJson.toString());
		//	Db.insertJson("CarFiles", jsonToInsert);

			featuresList = new ArrayList<Features>();
			featuresMap = new HashMap<String, Features>();
			carObject = new Cars();
			carObject.setFileName(filePath.substring(lastIndex + 1));
			if (row1.getCell(i + 3) == null || row1.getCell(i + 3).toString() == "") {
				i = i + 4;
			} else {
				i = i + 3;
			}
		}
		workbook.close();
	}
}