package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

/**
 * Created by sanketsarang on 05/11/18.
 */
public interface Validator {

    /**
     * Validates the specific case on the car object and returns errors if any found
     * @param carJson the JSON object representing the entire car data
     * @return empty string if validation passess, and appropriate error message if validation fails
     */
    public JSONObject validate(final JSONObject carJson);
}
