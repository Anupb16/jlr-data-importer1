package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlr.Excel_Importer_module.models.CarFiles;
import com.jlr.Excel_Importer_module.models.Cars;
import com.jlr.Excel_Importer_module.models.Features;
import com.jlr.Excel_Importer_module.models.SubFeatures;

public class MD5Calculator {

	public static void main(String[] args) throws Exception {
		
		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<String> filePaths = new ArrayList<String>();

		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");

		App listFiles = new App();
		filePaths = listFiles.listAllFiles(folder);
		for (String file : filePaths) {
			System.out.println("");
			System.out.println("file........" + file);
			processFile(file);
		}
		System.out.println("Done ..........");
	}
	
	@SuppressWarnings({ "unchecked" })
	public static void processFile(String filePath) throws Exception {

		List<JSONObject> masterMarkets = Db.searchAsJson(Query.select().from("MasterMarkets"));
		List<JSONObject> masterManufacturers = Db.searchAsJson(Query.select().from("MasterManufacturers"));
		List<JSONObject> masterModels = Db.searchAsJson(Query.select().from("MasterModels"));

		Map<String, String> markets = new HashMap<String, String>();
		Map<String, String> manufacturers = new HashMap<String, String>();
		Map<String, String> models = new HashMap<String, String>();

		for (JSONObject json : masterMarkets) {
			markets.put(json.getString("market"), json.getString("mid"));
		}

		for (JSONObject json : masterManufacturers) {
			manufacturers.put(json.getString("manufacturer"), json.getString("mfid"));
		}
		for (JSONObject json : masterModels) {
			models.put(json.getString("model"), json.getString("moid"));
		}
		
		Map<String, Features> featuresMap = new HashMap<String, Features>();
		Cars carObject = new Cars();
		Features features = new Features();
		CarFiles carFile = new CarFiles();
		List<SubFeatures> subFeatures = new ArrayList<SubFeatures>();
		List<Features> featuresList = new ArrayList<Features>();
		Set<String> featureNames = new HashSet<String>();

		int i = 0;
		int featureGroupstart = 2;
		int featureNamestart = 5;

		int lastIndex = filePath.lastIndexOf("\\");

		carObject.setFileName(filePath.substring(lastIndex + 1));
		carFile.setFileName(filePath.substring(lastIndex + 1));

		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);

		int firstRow = datatypeSheet.getFirstRowNum();

		Row row1 = datatypeSheet.getRow(firstRow + 1);

		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			datatypeSheet = workbook.getSheetAt(1);

			firstRow = datatypeSheet.getFirstRowNum();
			row1 = datatypeSheet.getRow(firstRow + 1);
			if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
				datatypeSheet = workbook.getSheetAt(2);

				firstRow = datatypeSheet.getFirstRowNum();
				row1 = datatypeSheet.getRow(firstRow + 1);
			}
		}

		Row row0 = datatypeSheet.getRow(3);

		if (row0.getCell(1) != null) {
			if (row0.getCell(1).toString().trim().equalsIgnoreCase("S")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("O")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("(O)")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("P")
					|| row0.getCell(1).toString().trim().equalsIgnoreCase("NA")) {
				featureGroupstart = 1;
				featureNamestart = 4;
			}
		}

		if (row0.getCell(3).toString() != null) {
			if (row0.getCell(3).toString().trim().equalsIgnoreCase("Model - Identifier")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("S")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("O")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("(O)")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("P")
					|| row0.getCell(3).toString().trim().equalsIgnoreCase("NA")) {
				featureGroupstart = 3;
				featureNamestart = 6;
			}
		}

		if (row1.getCell(5) != null && i == 0) {
			if (row1.getCell(5).toString().trim() != ""
					&& !row1.getCell(5).toString().trim().equalsIgnoreCase("Market")) {
				i = 5;
			}

		}
		if (row1.getCell(6) != null && i == 0) {
			if (row1.getCell(6).toString().trim() != ""
					&& !row1.getCell(6).toString().trim().equalsIgnoreCase("Market")) {
				i = 6;
			}
		}
		if (row1.getCell(7) != null && i == 0) {
			if (row1.getCell(7).toString().trim() != ""
					&& !row1.getCell(7).toString().trim().equalsIgnoreCase("Market")) {
				i = 7;
			}
		}
		if (row1.getCell(8) != null && i == 0) {
			if (row1.getCell(8).toString().trim() != ""
					&& !row1.getCell(8).toString().trim().equalsIgnoreCase("Market")) {
				i = 8;
			}
		}
		if (row1.getCell(9) != null && i == 0) {
			if (row1.getCell(9).toString().trim() != ""
					&& !row1.getCell(9).toString().trim().equalsIgnoreCase("Market")) {
				i = 9;
			}
		}
		List<JSONObject> carModelsList = Db.searchAsJson(Query.select().from("carmodels"));
		Map<String, Map<String,String>> featureCodeName =  new HashMap<String, Map<String,String>>();
		if(carModelsList.size() > 0) {
			
			carModelsList.forEach(carModel -> {
				try {
					featureCodeName.put(carModel.getString("market")+
							carModel.getString("manufacturer")+carModel.getString("model")+carModel.getString("trim"), new HashMap<>());
					//String cid = (String) carModel.remove("cid");
					String _id = (String) carModel.remove("_id");
					String createdDate = (String) carModel.remove("createdDate");
					if(carModel.getString("engineCapacity").equals("1999.0"))
				
					featureCodeName.get(carModel.getString("market")+
							carModel.getString("manufacturer")+carModel.getString("model")+carModel.getString("trim")).put(carModel.getString("cid"), getMD5(carModel.toString()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}

		while (i < row1.getLastCellNum()) {
			
			for (int rn = 1; rn < 11; rn++) {
				Row row = datatypeSheet.getRow(rn);

				if (row.getCell(featureNamestart) != null) {
					if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Market")) {
						carObject.setMarket(row.getCell(i).toString().trim());
						carObject.setMid(markets.get(row.getCell(i).toString().trim()));
						if(i==7) {
							carObject.setMid("7c1ce5b6-0794-4d25-a070-8632af74e079");
						}
					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Manufacturer")) {
						carObject.setManufacturer(row.getCell(i).toString().trim());
						carObject.setMfid(manufacturers.get(row.getCell(i).toString().trim()));
						if(i==7)
							carObject.setMfid("c23688b7-b179-409a-bb3b-6e7255289bb6");
					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model")) {
						if (row.getCell(i).toString().trim().equals("E-pace")
								|| row.getCell(i).toString().trim().equals("E-Pace")) {
							carObject.setModel("E-PACE");
						} else if (row.getCell(i).toString().trim().equals("I-Pace")) {
							carObject.setModel("I-PACE");
						} else if (row.getCell(i).toString().trim().equals("F-Pace")) {
							carObject.setModel("F-PACE");
						} else if (row.getCell(i).toString().trim().equals("5 series")) {
							carObject.setModel("5 Series");
						} else if (row.getCell(i).toString().trim().equals("S-Class")) {
							carObject.setModel("S Class");
						} else if (row.getCell(i).toString().trim().equals("Land Cruiser")) {
							carObject.setModel("Land Cruiser Prado");
                        } else {
							carObject.setModel(row.getCell(i).toString().trim());
						}
					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Version / trim")) {
						carObject.setTrim(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model Year")) {
						carObject.setModelYear(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Fuel Type")) {
						carObject.setFuel(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Price - RRP / MSRP")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("RRP/MSRP")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Price RRP / MSRP")
							|| row.getCell(featureNamestart).toString().trim()
									.equalsIgnoreCase("Price On road (RRP)")) {
						if (carObject.getPrice() == "" || carObject.getPrice() == null) {
							carObject.setPrice(row.getCell(i).toString().trim());
						}

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model code")) {
						carObject.setModelCode(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power PS HP / (KW)")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power PS (HP) / KW")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power")) {
						carObject.setPower(row.getCell(i).toString().trim());

					} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Engine Capacity (cc)")
							|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Engine Capacity")) {
						carObject.setEngineCapacity(row.getCell(i).toString().trim());
					}
				}
			}

			for (int rn = 11; rn < 31; rn++) {
				Row row = datatypeSheet.getRow(rn);
				features.setFeatureName("Specifications");
				featureNames.add(features.getFeatureName());
				SubFeatures subFeature = new SubFeatures();
				if (row != null) {

					if (row.getCell(1) == null) {
					} else {
						if (!row.getCell(1).toString().isEmpty()) {
							double d = Double.parseDouble(row.getCell(1).toString());
							int code = (int) d;
							subFeature.setFeatureCode(code);
						}
					}

					subFeature.setSubFeatureName(row.getCell(featureNamestart).toString());

					if (row.getCell(i) == null) {
						subFeature.setCategory("");
					} else {
						subFeature.setCategory(row.getCell(i).toString());
						if (row.getCell(featureNamestart).toString().equals("Body Type")) {
							carObject.setBodyType(row.getCell(i).toString().trim());
						}
					}

					if (row.getCell(i + 1) == null) {
						subFeature.setPrice("");
					} else
						subFeature.setPrice(row.getCell(i + 1).toString());

					if (row.getCell(i + 2) == null) {
						subFeature.setComment("");
					} else
						subFeature.setComment(row.getCell(i + 2).toString());
				}
			}
			features.setSubFeatures(subFeatures);
			featuresMap.put(features.getFeatureName(), features);

			int lastRowTocheck = datatypeSheet.getLastRowNum();
			if (lastRowTocheck > 579) {
				lastRowTocheck = 579; // changed from 580
			} else {
				lastRowTocheck = datatypeSheet.getLastRowNum();
			}

			for (int rn = 31; rn <= lastRowTocheck; rn++) {
				features = new Features();
				SubFeatures subFeature = new SubFeatures();
				subFeatures = new ArrayList<SubFeatures>();
				Row row = datatypeSheet.getRow(rn);
				if (row != null) {

					features.setFeatureName(row.getCell(featureGroupstart).toString());
					featureNames.add(features.getFeatureName());
					if (row.getCell(featureNamestart) != null) {

						if (row.getCell(1) == null) {
						} else {
							if (!row.getCell(1).toString().isEmpty()) {

								double d = Double.parseDouble(row.getCell(1).toString());
								int code = (int) d;
								subFeature.setFeatureCode(code);
							}
						}

						if (row.getCell(featureNamestart) == null) {
							subFeature.setSubFeatureName("");
						} else
							subFeature.setSubFeatureName(row.getCell(featureNamestart).toString());

						if (row.getCell(i) == null) {
							subFeature.setCategory("");
						} else
							subFeature.setCategory(row.getCell(i).toString());

						if (row.getCell(i + 1) == null) {
							subFeature.setPrice("");
						} else
							subFeature.setPrice(row.getCell(i + 1).toString());

						if (row.getCell(i + 2) == null) {
							subFeature.setComment("");
						} else
							subFeature.setComment(row.getCell(i + 2).toString());
					}
				}
				if (subFeature.getSubFeatureName() != "" && subFeature.getSubFeatureName() != null)
					subFeatures.add(subFeature);
				features.setSubFeatures(subFeatures);

				if (!features.getSubFeatures().isEmpty()) {
					if (featuresMap.containsKey(features.getFeatureName())) {
						Features featurePresent = featuresMap.get(features.getFeatureName());
						List<SubFeatures> subFeatures2 = featurePresent.getSubFeatures();
						subFeatures2.addAll(features.getSubFeatures());
						features.setSubFeatures(subFeatures2);
						features.set_id(UUID.randomUUID().toString());
						featuresMap.put(features.getFeatureName(), features);
					} else {
						features.set_id(UUID.randomUUID().toString());
						featuresMap.put(features.getFeatureName(), features);
					}
				}
			}

			for (String key : featuresMap.keySet()) {
				featuresList.add(featuresMap.get(key));
			}
			
			//carObject.setFeatureList(featuresList);
			JsonParser jsonParser = new JsonParser();
			//carObject.setCid(e.getKey());                  //TODO
			JSONObject json = new JSONObject(carObject);

			List<JSONObject> list = Db.searchAsJson(Query.select("type").from("CarTypes")
					.where(SearchParam.create("model").eq(json.getString("model").trim())));
			if (list.size() > 0) {
				json.put("segment", list.get(0).getString("type"));
			}
			
			JsonObject carsJson = (JsonObject) jsonParser.parse(json.toString());
		
			if (carObject.getMarket() != null && !carObject.getMarket().trim().equals(""))  {

				if(!featureCodeName.isEmpty()) {
					if(featureCodeName.containsKey(carObject.getMarket()+carObject.getManufacturer()+carObject.getModel()+carObject.getTrim())) {

						for (Entry<String, String> e : featureCodeName.get(carObject.getMarket()+carObject.getManufacturer()+carObject.getModel()+carObject.getTrim()).entrySet())  {
							
							if(e.getValue().equals(getMD5(carsJson.toString()))) {
						
							} else {
							
								//Db.remove(Cars.class, e.getKey());

								//carObject.setCid(e.getKey());
								carFile.setCarId(carObject.getCid());

								JSONObject json1 = new JSONObject(carObject);

								if (list.size() > 0) {
									json1.put("segment", list.get(0).getString("type"));
								}

								JsonObject carsJson1 = (JsonObject) jsonParser.parse(json1.toString());
								//Db.insertJson("carmodels", carsJson1);
							}
						}
					} /*else {
						carObject.setCid(UUID.randomUUID().toString());
						carFile.setCarId(carObject.getCid());

						JSONObject json2 = new JSONObject(carObject);

						if (list.size() > 0) {
							json2.put("segment", list.get(0).getString("type"));
						}

						JsonObject carsJson2 = (JsonObject) jsonParser.parse(json2.toString());
						Db.insertJson("carmodels", carsJson2);
					}*/
				} else {
					carObject.setCid(UUID.randomUUID().toString());
					carFile.setCarId(carObject.getCid());

					JSONObject json2 = new JSONObject(carObject);
					if (list.size() > 0) {
						json2.put("segment", list.get(0).getString("type"));
					}

					JsonObject carsJson2 = (JsonObject) jsonParser.parse(json2.toString());
					//Db.insertJson("carmodels", carsJson2);
				}
			}
				
	//	    JSONObject fileJson = new JSONObject(carFile);
	//		JsonObject jsonToInsert = (JsonObject) jsonParser.parse(fileJson.toString());
	//		Db.insertJson("CarFiles", jsonToInsert);

			featuresList = new ArrayList<Features>();
			featuresMap = new HashMap<String, Features>();
			carObject = new Cars();
			carObject.setFileName(filePath.substring(lastIndex + 1));
			if (row1.getCell(i + 3) == null || row1.getCell(i + 3).toString() == "") {
				i = i + 4;
			} else {
				i = i + 3;
			}
		}
		workbook.close();
	}

	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
}