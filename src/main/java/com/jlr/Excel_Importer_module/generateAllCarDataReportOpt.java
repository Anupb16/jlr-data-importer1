package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;

public class generateAllCarDataReportOpt {

	static String[] columns = { "New S.No 1.2", "Uniq. Ref. No.", "Feature Group", "NEW CFVS name v1.2", "Description",
			"Feature Name", "Source" };
	// static String filename = ""; // TODO remove
	// static int col; // TO remove
	static Map<String, Boolean> masterFeaturesMergeMap = new HashMap<>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<JSONObject> OrderedMasterFeaturesList = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));

		Collections.sort(OrderedMasterFeaturesList, new Comparator<JSONObject>() {
			@Override
			public int compare(JSONObject o1, JSONObject o2) {
				try {
					return o1.getInt("uniqueSerialNo") - o2.getInt("uniqueSerialNo");
				} catch (JSONException e) {
					e.printStackTrace();
					return 0;
				}
			}
		});

		OrderedMasterFeaturesList.forEach(m -> {
			masterFeaturesMergeMap.put(m.getString("featureCode"), m.getBoolean("merged"));
		});
		List<JSONObject> masterModelsNames = Db.searchAsJson(Query.select().from("MasterModels"));

		masterModelsNames.forEach(item -> {
			try {
				List<JSONObject> cars = new ArrayList<>();
				cars = Db.searchAsJson(Query.select().from("CarModels")
						.where(SearchParam.create("moid").eq(item.getString("moid"))
								.and(SearchParam.create("manufacturer").noteq("Land Rover"))
								.and(SearchParam.create("manufacturer").noteq("Jaguar"))));

				if (cars.isEmpty()) {
					return;
				}

				Collections.sort(cars, new Comparator<JSONObject>() {
					@Override
					public int compare(JSONObject o1, JSONObject o2) {
						try {
							return o1.getString("market").compareTo(o2.getString("market"));
						} catch (JSONException e) {
							e.printStackTrace();
							return 0;
						}
					}
				});

				Workbook workbook = new XSSFWorkbook();
				Sheet sheet = workbook.createSheet("Benchmark");
				Row headerRow = sheet.createRow(0);
				for (int i = 0; i < columns.length; i++) {
					Cell cell = headerRow.createCell(i);
					cell.setCellValue(columns[i]);
				}

				final RowCounter rc = new RowCounter();
				OrderedMasterFeaturesList.forEach(masterFeature -> {
					Row row = sheet.createRow(rc.getRow());
					row.createCell(0).setCellValue(rc.getRow());
					row.createCell(1).setCellValue(masterFeature.getString("featureCode"));
					row.createCell(2).setCellValue(masterFeature.getString("featureGroup"));
					row.createCell(3).setCellValue("");
					row.createCell(4).setCellValue("");
					row.createCell(5).setCellValue(masterFeature.getString("featureName"));
					row.createCell(6).setCellValue(masterFeature.getBoolean("specAdjustable"));
					rc.increment();
				});
				// Row row1 = sheet.createRow(rc.getRow()); // TODO remove

				final ColumnCounter columnCounter = new ColumnCounter();

				cars.forEach(car -> {
					RowCounter rowCounter = new RowCounter();

					OrderedMasterFeaturesList.forEach(masterFeature -> {
						Row row = sheet.getRow(rowCounter.getRow());

						JSONObject featuresJson = car.getJSONObject("features");
						String featureCode = masterFeature.getString("featureCode");
						if (featuresJson.has(featureCode)) {
							if (!masterFeaturesMergeMap.containsKey(featureCode)) {
								return;
							}
							if (masterFeaturesMergeMap.get(featureCode) == false) {
								row.createCell(columnCounter.getColumn()).setCellValue(featuresJson.getJSONObject(featureCode).has("category") ? featuresJson.getJSONObject(featureCode).get("category").toString(): "");
								row.createCell(columnCounter.getColumn() + 1).setCellValue(featuresJson.getJSONObject(featureCode).has("price") ? featuresJson.getJSONObject(featureCode).get("price").toString(): "");
								row.createCell(columnCounter.getColumn() + 2).setCellValue(featuresJson.getJSONObject(featureCode).has("comment") ? featuresJson.getJSONObject(featureCode).get("comment").toString(): "");
								setBackgroundColor(workbook, columnCounter, row);
							} else {
								row.createCell(columnCounter.getColumn()).setCellValue(featuresJson.getJSONObject(featureCode).has("category") ? featuresJson.getJSONObject(featureCode).get("category").toString(): "");
								sheet.addMergedRegion(new CellRangeAddress(rowCounter.getRow(), rowCounter.getRow(), columnCounter.getColumn(), columnCounter.getColumn() + 2));

								String commentTxt  =featuresJson.getJSONObject(featureCode).has("comment") ? featuresJson.getJSONObject(featureCode).get("comment").toString(): "";
								if(!commentTxt.equals("")) {
									CreationHelper factory = workbook.getCreationHelper();
									Drawing drawing = sheet.createDrawingPatriarch();
									ClientAnchor anchor = factory.createClientAnchor();
									RichTextString str = factory.createRichTextString(commentTxt);
									Comment comment = drawing.createCellComment(anchor);
									comment.setString(str);
									row.getCell(columnCounter.getColumn()).setCellComment(comment);
								}	

							}
							// filename = car.getString("fileName"); // TO remove
							// col = columnCounter.getColumn(); // TODO remove
						}
						rowCounter.increment();
					});
					// Row row = sheet.getRow(rowCounter.getRow()); // TO remove
					// row.createCell(col).setCellValue(filename); // TODO remove

					columnCounter.increment();
				});

				File myFile = new File("/mnt/drive-download/"
						+ item.getString("model") + ".xlsx");
				FileOutputStream fileOut = new FileOutputStream(myFile);
				workbook.write(fileOut);
				fileOut.close();
				workbook.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		System.out.println("done...");
	}

	private static void setBackgroundColor(Workbook workbook, ColumnCounter col, Row row) {
		XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();

		if (row.getCell(col.getColumn()).toString().equals("S")) {
			setCellStyle(style1, IndexedColors.GREEN.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col.getColumn()).toString().equals("NA")) {
			setCellStyle(style1, IndexedColors.DARK_RED.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col.getColumn()).toString().equals("O")) {
			setCellStyle(style1, IndexedColors.DARK_YELLOW.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col.getColumn()).toString().equals("P")) {
			setCellStyle(style1, IndexedColors.BLUE_GREY.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col.getColumn()).toString().equals("(O)")) {
			setCellStyle(style1, IndexedColors.WHITE1.getIndex(), FillPatternType.SOLID_FOREGROUND);
		}
		row.getCell(col.getColumn()).setCellStyle(style1);
	}

	private static void setCellStyle(XSSFCellStyle style1, short colorIndex, FillPatternType solidForeground) {
		style1.setFillForegroundColor(colorIndex);
		style1.setFillPattern(solidForeground);
	}
}

class ColumnCounter {
	private int column;

	public ColumnCounter() {
		this.column = 7;
	}

	public void increment() {
		this.column += 3;
	}

	public int getColumn() {
		return this.column;
	}
}

class RowCounter {
	private int row = 1;

	public void increment() {
		this.row += 1;
	}

	public int getRow() {
		return this.row;
	}
}