package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateDriverConditionMonitor implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");
		
			/*if(features.getJSONObject("520").getString("category").isEmpty()) {
	            return "for Driver Condition Response category cannot be blank";
	        }*/

	        if(features.getJSONObject("520").getString("category").equals("S")) {
				if(!features.getJSONObject("519").getString("category").equals("NA")) {
					
					features.getJSONObject("519").put("category", "NA");
					carJson.put("features", features);
					return carJson;
					//return "Driver Condition Monitor must be NA if Driver Condition Response is S";
				}
			}

        return new JSONObject().put("error", "");
	}

}
