package com.jlr.cat.importer;

import com.asprise.ocr.Ocr;

import java.io.File;

/**
 * Created by sanketsarang on 23/10/18.
 */
public class Test {

    public static void main(String[] args) {
        Ocr.setUp(); // one time setup
        Ocr ocr = new Ocr(); // create a new OCR engine
        ocr.startEngine("eng", Ocr.SPEED_FASTEST, "START_PROP_DICT_CUSTOM_TEMPLATES_FILE=/Users/sanketsarang/Desktop/ocr-template1.txt"); // English
        String s = ocr.recognize(new File[] {new File("/Users/sanketsarang/Desktop/ocr-test3.png")}, Ocr.RECOGNIZE_TYPE_TEXT, Ocr.OUTPUT_FORMAT_PLAINTEXT);
        System.out.println("\n\nOCR Result will now show");
        System.out.println("Result: " + s);
// ocr more images here ...
        ocr.stopEngine();
    }
}
