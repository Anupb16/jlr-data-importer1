package com.jlr.Excel_Importer_module.models;

public class Trim {
	private String type;
	private String modelYear;
	private String carId;
	private String fuel;
	private String modelCode;
	
	public Trim() {
		// TODO Auto-generated constructor stub
	}
	
	public Trim(String type, String modelYear, String carId, String fuel, String modelCode) {
		super();
		this.type = type;
		this.modelYear = modelYear;
		this.carId = carId;
		this.fuel = fuel;
		this.modelCode = modelCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getModelYear() {
		return modelYear;
	}
	public void setModelYear(String modelYear) {
		this.modelYear = modelYear;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getFuel() {
		return fuel;
	}
	public void setFuel(String fuel) {
		this.fuel = fuel;
	}
	public String getModelCode() {
		return modelCode;
	}
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

}
