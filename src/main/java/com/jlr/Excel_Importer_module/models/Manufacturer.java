package com.jlr.Excel_Importer_module.models;


public class Manufacturer{

	private String manufacturer;
	private String mfid;
	private String mid;
	
	public Manufacturer() {
		// TODO Auto-generated constructor stub
	}

	public Manufacturer(String manufacturer, String mfid, String mid) {
		super();
		this.manufacturer = manufacturer;
		this.mfid = mfid;
		this.mid = mid;
	}



	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getMfid() {
		return mfid;
	}

	public void setMfid(String mfid) {
		this.mfid = mfid;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
	
}
