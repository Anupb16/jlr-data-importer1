package com.jlr.Excel_Importer_module.models;


public class Market{

	private String market;
	private String mid;
	
	public Market() {
		// TODO Auto-generated constructor stub
	}

	public Market(String market, String mid) {
		super();
		this.market = market;
		this.mid = mid;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}
	
}
