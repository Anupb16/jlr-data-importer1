package com.jlr.Excel_Importer_module;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;

public class generateOxoFeatureMapping {

	List<String> files = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		String[] columns = { "RADSID", "FeatureGroup", "SystemDescription", "JaguarDescription", "LandRoverDescription", "Applicability",
				"exactCodes" };

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<JSONObject> oxoMasters = Db.searchAsJson(Query.select().from("OxoMasterFeatureMapping"));

		Workbook workbook = new HSSFWorkbook();

		Sheet sheet = workbook.createSheet("OxoFeatureMapping");

		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		int rowNum = 1;
		for (JSONObject JSON : oxoMasters) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(JSON.get("radsId").toString());
			row.createCell(1).setCellValue(JSON.get("featureGroup").toString());
			row.createCell(2).setCellValue(JSON.get("systemDescription").toString());
			row.createCell(3).setCellValue(JSON.get("jaguarDescriptions").toString());
			row.createCell(4).setCellValue(JSON.get("landRoverDescription").toString());
			row.createCell(5).setCellValue(JSON.get("applicability").toString());
			row.createCell(6).setCellValue(JSON.get("exactCodes").toString());
		}

//		for (int i = 0; i < columns.length; i++) {
//			sheet.autoSizeColumn(i);
//		}

		FileOutputStream fileOut = new FileOutputStream(
				"/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData/OxoMasterReport.xlsx");
		workbook.write(fileOut);
		fileOut.close();

		workbook.close();
	}
}