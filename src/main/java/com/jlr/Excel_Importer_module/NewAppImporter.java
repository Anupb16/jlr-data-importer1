package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;
import com.blobcity.json.JSON;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class NewAppImporter {

//	static Map<String, String> markets = new HashMap<String, String>();
//	static Map<String, String> manufacturers = new HashMap<String, String>();
//	static Map<String, String> models = new HashMap<String, String>();
//	List<String> files = new ArrayList<String>();
//	static List<String> duplicateRecords  = new ArrayList<String>();
//
//	@SuppressWarnings("unchecked")
//	public static void main(String[] args) throws Exception {
//
//		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");
//
//		List<JSONObject> masterMarkets = Db.searchAsJson(Query.select().from("MasterMarkets"));
//		List<JSONObject> masterManufacturers = Db.searchAsJson(Query.select().from("MasterManufacturers"));
//		List<JSONObject> masterModels = Db.searchAsJson(Query.select().from("MasterModels"));
//
//		for (JSONObject json : masterMarkets) {
//			markets.put(json.getString("market"), json.getString("mid"));
//		}
//
//		for (JSONObject json : masterManufacturers) {
//			manufacturers.put(json.getString("manufacturer"), json.getString("mfid"));
//		}
//		for (JSONObject json : masterModels) {
//			models.put(json.getString("model"), json.getString("moid"));
//		}
//
//		List<String> filePaths = new ArrayList<String>();
//
//		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");
//
//		NewAppImporter listFiles = new NewAppImporter();
//		AppValidator validator = new AppValidator();
//
//		filePaths = listFiles.listAllFiles(folder);
//
//		for (String file : filePaths) {
//			System.out.println('\n' + "File Name......  " + file);
//			validator.validateFiles(file);
//			processFile(file);
//		}
//		System.out.println("DONE PROCESSING......");
//
//		duplicateRecords.forEach(System.out::println);
//	}
//
//	public List<String> listAllFiles(File folder) {
//		try {
//			File[] fileNames = folder.listFiles();
//			for (File file : fileNames) {
//				if (file.isDirectory()) {
//					listAllFiles(file);
//				} else {
//					files.addAll(readContent(file));
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return files;
//	}
//
//	public List<String> readContent(File file) throws IOException {
//		List<String> files = new ArrayList<String>();
//		if (!file.getCanonicalPath().contains("Archive") && !file.getCanonicalPath().contains("archive")
//				&& !file.getCanonicalPath().contains(".DS_Store"))
//			files.add(file.getCanonicalPath());
//		return files;
//	}
//
//	@SuppressWarnings({ "unchecked" })
//	public static void processFile(String filePath) throws Exception {
//		try {
//			JsonObject carsJson = new JsonObject();
//			JsonObject jsn1 = new JsonObject();
//
//			int i = 0;
//			int featureGroupstart = 2;
//			int featureNamestart = 5;
//
//			carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
//
//			FileInputStream excelFile = new FileInputStream(new File(filePath));
//			Workbook workbook = new XSSFWorkbook(excelFile);
//			Sheet datatypeSheet = workbook.getSheetAt(0);
//
//			int firstRow = datatypeSheet.getFirstRowNum();
//			Row row1 = datatypeSheet.getRow(firstRow + 1);
//
//			if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
//				datatypeSheet = workbook.getSheetAt(1);
//				firstRow = datatypeSheet.getFirstRowNum();
//				row1 = datatypeSheet.getRow(firstRow + 1);
//				if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
//					datatypeSheet = workbook.getSheetAt(2);
//					firstRow = datatypeSheet.getFirstRowNum();
//					row1 = datatypeSheet.getRow(firstRow + 1);
//				}
//			}
//
//			int lastRowTocheck = datatypeSheet.getLastRowNum();
//			if (lastRowTocheck > 579) {
//				lastRowTocheck = 579;
//			} else {
//				lastRowTocheck = datatypeSheet.getLastRowNum();
//			}
//
//			Row row0 = datatypeSheet.getRow(3);
//
//			if (row0.getCell(1) != null) {
//				if (row0.getCell(1).toString().trim().equalsIgnoreCase("S")
//						|| row0.getCell(1).toString().trim().equalsIgnoreCase("O")
//						|| row0.getCell(1).toString().trim().equalsIgnoreCase("(O)")
//						|| row0.getCell(1).toString().trim().equalsIgnoreCase("P")
//						|| row0.getCell(1).toString().trim().equalsIgnoreCase("NA")) {
//					featureGroupstart = 1;
//					featureNamestart = 4;
//				}
//			}
//
//			if (row0.getCell(3).toString() != null) {
//				if (row0.getCell(3).toString().trim().equalsIgnoreCase("Model - Identifier")
//						|| row0.getCell(3).toString().trim().equalsIgnoreCase("S")
//						|| row0.getCell(3).toString().trim().equalsIgnoreCase("O")
//						|| row0.getCell(3).toString().trim().equalsIgnoreCase("(O)")
//						|| row0.getCell(3).toString().trim().equalsIgnoreCase("P")
//						|| row0.getCell(3).toString().trim().equalsIgnoreCase("NA")) {
//					featureGroupstart = 3;
//					featureNamestart = 6;
//				}
//			}
//
//			if (row1.getCell(5) != null && i == 0) {
//				if (row1.getCell(5).toString().trim() != ""
//						&& !row1.getCell(5).toString().trim().equalsIgnoreCase("Market")) {
//					i = 5;
//				}
//			}
//			if (row1.getCell(6) != null && i == 0) {
//				if (row1.getCell(6).toString().trim() != ""
//						&& !row1.getCell(6).toString().trim().equalsIgnoreCase("Market")) {
//					i = 6;
//				}
//			}
//			if (row1.getCell(7) != null && i == 0) {
//				if (row1.getCell(7).toString().trim() != ""
//						&& !row1.getCell(7).toString().trim().equalsIgnoreCase("Market")) {
//					i = 7;
//				}
//
//			}
//			if (row1.getCell(8) != null && i == 0) {
//				if (row1.getCell(8).toString().trim() != ""
//						&& !row1.getCell(8).toString().trim().equalsIgnoreCase("Market")) {
//					i = 8;
//				}
//			}
//			if (row1.getCell(9) != null && i == 0) {
//				if (row1.getCell(9).toString().trim() != ""
//						&& !row1.getCell(9).toString().trim().equalsIgnoreCase("Market")) {
//					i = 9;
//				}
//			}
//
//			while (i < row1.getLastCellNum()) {
//				for (int rn = 1; rn < 11; rn++) {
//					Row row = datatypeSheet.getRow(rn);
//
//					if (row.getCell(featureNamestart) != null) {
//
//						if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Market")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("market", row.getCell(i).toString().trim());
//								carsJson.addProperty("mid", markets.get(row.getCell(i).toString().trim()));
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Manufacturer")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								if (!row.getCell(i).toString().trim().equalsIgnoreCase("Land Rover")
//										&& !row.getCell(i).toString().trim().equalsIgnoreCase("Jaguar")) {
//									carsJson.addProperty("manufacturer", row.getCell(i).toString().trim());
//									carsJson.addProperty("mfid", manufacturers.get(row.getCell(i).toString().trim()));
//								}
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								if (models.containsKey(row.getCell(i).toString().trim())) {
//									carsJson.addProperty("model", row.getCell(i).toString().trim());
//									carsJson.addProperty("moid", models.get(row.getCell(i).toString().trim()));
//								} else {
//									if (row.getCell(i).toString().trim().equals("E-pace")
//											|| row.getCell(i).toString().trim().equals("E-Pace")) {
//										carsJson.addProperty("model", "E-PACE");
//										carsJson.addProperty("moid", models.get("E-PACE"));
//
//									} else if (row.getCell(i).toString().trim().equals("I-Pace")) {
//										carsJson.addProperty("model", "I-PACE");
//										carsJson.addProperty("moid", models.get("I-PACE"));
//
//									} else if (row.getCell(i).toString().trim().equals("F-Pace")) {
//										carsJson.addProperty("model", "F-PACE");
//										carsJson.addProperty("moid", models.get("F-PACE"));
//
//									} else if (row.getCell(i).toString().trim().equals("5 series")) {
//										carsJson.addProperty("model", "5 Series");
//										carsJson.addProperty("moid", models.get("5 Series"));
//
//									} else if (row.getCell(i).toString().trim().equals("S-Class")) {
//										carsJson.addProperty("model", "S Class");
//										carsJson.addProperty("moid", models.get("S Class"));
//
//									} else if (row.getCell(i).toString().trim().equals("Land Cruiser")) {
//										carsJson.addProperty("model", "Land Cruiser Prado");
//										carsJson.addProperty("moid", models.get("Land Cruiser Prado"));
//
//									} else if (row.getCell(i).toString().trim().equals("911.0")) {
//										carsJson.addProperty("model", "911");
//										carsJson.addProperty("moid", models.get("911"));
//									}
//								}
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Version / trim")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("trim", row.getCell(i).toString().trim());
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model Year")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("modelYear",
//										String.valueOf(row.getCell(i)).trim().split("\\.")[0].substring(2, 4) + "MY");
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Fuel Type")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("fuel", row.getCell(i).toString().trim());
//							}
//						} else if (row.getCell(featureNamestart).toString().trim()
//								.equalsIgnoreCase("Price - RRP / MSRP")
//								|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("RRP/MSRP")
//								|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Price RRP / MSRP")
//								|| row.getCell(featureNamestart).toString().trim()
//										.equalsIgnoreCase("Price On road (RRP)")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								try {
//									Double dValue = Double.parseDouble(String.valueOf(row.getCell(i)));
//									if (dValue != null) {
//										carsJson.addProperty("price", Long.toString(Math.round(dValue)).trim());
//									}
//								} catch (NumberFormatException e) {
//									carsJson.addProperty("price", String.valueOf(row.getCell(i)).trim());
//								}
//							}
//						} else if (row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model code")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("modelCode", row.getCell(i).toString().trim());
//							} else
//								carsJson.addProperty("modelCode", "");
//						} else if (row.getCell(featureNamestart).toString().trim()
//								.equalsIgnoreCase("Power PS HP / (KW)")
//								|| row.getCell(featureNamestart).toString().trim()
//										.equalsIgnoreCase("Power PS (HP) / KW")
//								|| row.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Power")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								carsJson.addProperty("power", row.getCell(i).toString().trim());
//							}
//						} else if (row.getCell(featureNamestart).toString().trim()
//								.equalsIgnoreCase("Engine Capacity (cc)")
//								|| row.getCell(featureNamestart).toString().trim()
//										.equalsIgnoreCase("Engine Capacity")) {
//							if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//								try {
//									Double dValue = Double.parseDouble(String.valueOf(row.getCell(i)));
//									if (dValue != null) {
//										carsJson.addProperty("engineCapacity",
//												Long.toString(Math.round(dValue)).trim());
//									}
//								} catch (NumberFormatException e) {
//									carsJson.addProperty("engineCapacity", String.valueOf(row.getCell(i)).trim());
//								}
//							}
//						}
//					}
//				}
//
//				for (int rn = 1; rn <= lastRowTocheck; rn++) {
//					String cellComment = "";
//					String comment = "";
//					JsonObject jsn = new JsonObject();
//					Row row = datatypeSheet.getRow(rn);
//					int code = 0;
//
//					if (row != null) {
//						if (row.getCell(1) == null) {
//						} else {
//							if (!row.getCell(1).toString().isEmpty()) {
//								double d = Double.parseDouble(row.getCell(1).toString());
//								code = (int) d;
//							}
//						}
//
//						if (row.getCell(i) == null || row.getCell(i).toString().equals("")) {
//							jsn.addProperty("category", "");
//						} else {
//							if (row.getCell(featureNamestart).toString().equals("Body Type")) {
//								carsJson.addProperty("bodyType", row.getCell(i).toString().trim());
//							}
//							if (row.getCell(featureNamestart).toString().equals("Model Year")) {
//								if (row.getCell(i) != null && row.getCell(i).toString() != "") {
//									jsn.addProperty("category",
//											String.valueOf(row.getCell(i)).trim().split("\\.")[0].substring(2, 4)
//													+ "MY");
//								}
//							} else {
//								if (rn == 26) {
//									if (row.getCell(i).toString().equals("EU6")) {
//										jsn.addProperty("category", "Euro 6");
//									} else {
//										jsn.addProperty("category", String.valueOf(row.getCell(i)).trim());
//									}
//								} else if (rn == 14 || rn == 17) {
//									jsn.addProperty("category", String.valueOf(row.getCell(i)).trim());
//								} else {
//									try {
//										Double dValue = Double.parseDouble(String.valueOf(row.getCell(i)));
//										if (dValue != null) {
//											jsn.addProperty("category", Long.toString(Math.round(dValue)).trim());
//										}
//									} catch (NumberFormatException e) {
//										jsn.addProperty("category", String.valueOf(row.getCell(i)).trim());
//									}
//								}
//							}
//							if (row.getCell(i).getCellComment() != null) {
//								cellComment = cellComment + " "
//										+ String.valueOf(row.getCell(i).getCellComment().getString() + " ").trim();
//							}
//						}
//
//						if (row.getCell(i + 1) == null) {
//							jsn.addProperty("price", "");
//						} else {
//							try {
//								Double dValue = Double.parseDouble(String.valueOf(row.getCell(i + 1)));
//								if (dValue != null) {
//									jsn.addProperty("price", Long.toString(Math.round(dValue)).trim());
//								}
//							} catch (NumberFormatException e) {
//								jsn.addProperty("price", String.valueOf(row.getCell(i + 1)).trim());
//							}
//
//							if (row.getCell(i + 1).getCellComment() != null) {
//								cellComment = cellComment + " "
//										+ String.valueOf(row.getCell(i + 1).getCellComment().getString() + " ").trim();
//							}
//						}
//
//						if (row.getCell(i + 2) == null) {
//							jsn.addProperty("comment", "");
//						} else {
//							comment = row.getCell(i + 2).toString().trim();
//							if (row.getCell(i + 2).getCellComment() != null) {
//								cellComment = cellComment + " "
//										+ String.valueOf(row.getCell(i + 2).getCellComment().getString() + " ").trim();
//							}
//						}
//						jsn.addProperty("comment", comment + cellComment);
//					}
//
//					jsn1.add(String.valueOf(code), jsn);
//				}
//				String pattern = "dd/MMM/yyyy";
//				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//				String date = simpleDateFormat.format(new Date());
//
//				JsonObject jsn = new JsonObject();
//				jsn.addProperty("category", date);
//				jsn.addProperty("price", "");
//				jsn.addProperty("comment", "");
//
//				jsn1.add("583", jsn);
//
//				carsJson.add("features", jsn1);
//				carsJson.addProperty("version", date);
//
//				if (carsJson.get("model") != null) {
//					List<JsonObject> list = Db.searchAsJson(Query.select("type").from("CarTypes")
//							.where(SearchParam.create("model").eq(carsJson.get("model").toString().trim())));
//					if (list.size() > 0) {
//						carsJson.add("segment", list.get(0).get("type"));
//					} else
//						carsJson.addProperty("segment", "");
//				}
//
//				if (carsJson.get("market") != null && carsJson.get("manufacturer") != null
//						&& carsJson.get("model") != null) {
//					if (markets.containsKey(carsJson.get("market").getAsString())) {
//						if (manufacturers.containsKey(carsJson.get("manufacturer").getAsString())) {
//							if (models.containsKey(carsJson.get("model").getAsString())) {
//
//								String jsonCarObjectName = carsJson.get("market").getAsString()
//										+ carsJson.get("manufacturer").getAsString()
//										+ carsJson.get("model").getAsString() + carsJson.get("trim").getAsString()
//										+ carsJson.get("modelYear").getAsString() + carsJson.get("fuel").getAsString() + carsJson.get("power").getAsString();
//
//								CarModelsCacheData cacheData = new CarModelsCacheData(
//										carsJson.get("market").getAsString(),
//										carsJson.get("manufacturer").getAsString(), carsJson.get("model").getAsString(),
//										carsJson.get("trim").getAsString(), carsJson.get("modelYear").getAsString(),
//										carsJson.get("fuel").getAsString(), carsJson.get("power").getAsString());
//
//								if (cacheData.containsModel(jsonCarObjectName)) {
//
//									JSONObject obj = cacheData.getModel(jsonCarObjectName);
//									obj.remove("cid");
//									String _id = (String) obj.remove("_id");
//									obj.remove("parCode");
//									String dbFilename = (String)obj.remove("fileName");
//									JsonElement carJsonFileName = carsJson.remove("fileName");
//									String filename = carJsonFileName.toString();
//									if (!JSON.areEqual(carsJson.toString(), obj.toString())) {
//
//										if (carsJson.get("version").getAsString().equals(obj.get("version").toString())) {
//											duplicateRecords.add(carsJson.get("market").getAsString()+"  "+carsJson.get("manufacturer").getAsString()+"  "+carsJson.get("model").getAsString()+"  "+carsJson.get("trim").getAsString()+"  "+carsJson.get("modelYear").getAsString()+"  "+carsJson.get("fuel").getAsString()+"  "+carsJson.get("power").getAsString()+"   "+filename+"   "+dbFilename);
//											Db.execute("delete from `jlr-db-sb`.`CarModels` where `_id`= '" + _id + "'");
//											carsJson.addProperty("cid", UUID.randomUUID().toString());
//											carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
//											Db.insertJson("CarModels", carsJson);
//
//										} else {
//											carsJson.addProperty("cid", UUID.randomUUID().toString());
//											carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
//											Db.insertJson("CarModels", carsJson);
//										}
//									}
//								} else {
//									String cid = UUID.randomUUID().toString();
//									carsJson.addProperty("cid", cid);
//									Db.insertJson("CarModels", carsJson);
//								}
//							}
//						}
//					}
//				}
//
//				carsJson = new JsonObject();
//				carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
//				if (row1.getCell(i + 3) == null || row1.getCell(i + 3).toString() == "") {
//					i = i + 4;
//				} else {
//					i = i + 3;
//				}
//			}
//			workbook.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}