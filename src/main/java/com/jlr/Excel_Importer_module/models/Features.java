package com.jlr.Excel_Importer_module.models;

import java.util.List;

import com.blobcity.db.Db;
import com.blobcity.db.annotations.Entity;

@Entity(ds = "Cars")
public class Features extends Db {

	private String FeatureName;
	private List<SubFeatures> subFeatures;
	
	public Features() {
		// TODO Auto-generated constructor stub
	}

	public Features(String featureName, List<SubFeatures> subFeatures) {
		super();
		FeatureName = featureName;
		this.subFeatures = subFeatures;
	}

	public String getFeatureName() {
		return FeatureName;
	}

	public void setFeatureName(String featureName) {
		FeatureName = featureName;
	}

	public List<SubFeatures> getSubFeatures() {
		return subFeatures;
	}

	public void setSubFeatures(List<SubFeatures> subFeatures) {
		this.subFeatures = subFeatures;
	}	
}