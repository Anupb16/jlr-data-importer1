package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlr.Excel_Importer_module.models.FullFeatureMatrix;

public class MFDImporter {

	List<String> files = new ArrayList<String>();

	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<String> filePaths = new ArrayList<String>();

		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");

		MFDImporter listFiles = new MFDImporter();

		filePaths = listFiles.listAllFiles(folder);
		for (String file : filePaths) {
			processFile(file);
		}
	}

	public List<String> listAllFiles(File folder) {

		File[] fileNames = folder.listFiles();
		for (File file : fileNames) {
			if (file.isDirectory()) {
				listAllFiles(file);
			} else {
				try {
					files.addAll(readContent(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return files;
	}

	public List<String> readContent(File file) throws IOException {
		List<String> files = new ArrayList<String>();

		if (file.getCanonicalPath().contains("Archive") || file.getCanonicalPath().contains("archive")) {
		} else if (file.getCanonicalPath().contains(".DS_Store")) {
		} else {
			files.add(file.getCanonicalPath());
		}
		return files;
	}

	public static void processFile(String filePath) throws Exception {

		FullFeatureMatrix featureMatrix = null;

		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(1);
		int lastRow = datatypeSheet.getLastRowNum();
		Row row6 = datatypeSheet.getRow(6);

		List<JsonObject> OxoMasterList = new ArrayList<>();
		for (int rn = 8; rn <= lastRow; rn++) {
			Row row = datatypeSheet.getRow(rn);

			featureMatrix = new FullFeatureMatrix();
			for (int col = 0; col < row6.getLastCellNum(); col++) {

				if (row6.getCell(col).toString().trim().equalsIgnoreCase("RADS ID")) {
					featureMatrix.setModelCode(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Feature Group")) {
					featureMatrix.setFeatureGroup(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("JLR Code")) {
					featureMatrix.setJlrCode(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("OA Code")) {
					featureMatrix.setOaCode(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("WERS Code")) {
					featureMatrix.setWersCodes(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("System Description")) {
					featureMatrix.setSystemDescription(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Jaguar Description")) {
					featureMatrix.setJaguarDescriptions(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Land Rover Description")) {
					featureMatrix.setLandRoverDescription(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Long Description")) {
					featureMatrix.setLongDescription(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("EFG Code")) {
					featureMatrix.setEfgCode(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("EFG Type")) {
					featureMatrix.setEfgType(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("EFG Description")) {
					featureMatrix.setEfgDescription(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Collector Code")) {
					featureMatrix.setCollectorCode(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Configuration Group")) {
					featureMatrix.setConfigurationGroup(row.getCell(col).toString().trim());

				} else if (row6.getCell(col).toString().trim().equalsIgnoreCase("Applicability")) {
					featureMatrix.setApplicability(row.getCell(col).toString().trim());

				}
			}
			JSONObject json = new JSONObject(featureMatrix);
			if (!row.getCell(1).toString().trim().equals("")) {
				JsonParser jsonParser = new JsonParser();
				JsonObject oxoMasterJson = (JsonObject) jsonParser.parse(json.toString());
				OxoMasterList.add(oxoMasterJson);
			}
		}
		Db.insertJson("OxoMaster", OxoMasterList);
		workbook.close();
	}
}