package com.jlr.cat.importer;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;
import com.blobcity.json.JSON;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jlr.Excel_Importer_module.AppValidatorOpt;
import com.jlr.Excel_Importer_module.CarModelsCacheData;
import com.jlr.Excel_Importer_module.NewAppImporterOpt;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author sanketsarang
 */
public class SingleSheetImporter {

    private final Map<String, String> markets = new HashMap<String, String>();
    private final Map<String, String> manufacturers = new HashMap<String, String>();
    private final Map<String, String> models = new HashMap<String, String>();

    private final List<String> duplicateRecords = new ArrayList<String>();
    private List<JSONObject> OrderedMasterFeaturesList;
    private final Map<String, Boolean> masterFeaturesMergeMap = new HashMap<>();

    public void init() {
        List<JSONObject> masterMarkets = Db.searchAsJson(Query.select().from("MasterMarkets"));
        List<JSONObject> masterManufacturers = Db.searchAsJson(Query.select().from("MasterManufacturers"));
        List<JSONObject> masterModels = Db.searchAsJson(Query.select().from("MasterModels"));

        OrderedMasterFeaturesList = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));
        Collections.sort(OrderedMasterFeaturesList, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject o1, JSONObject o2) {
                try {
                    return o1.getInt("uniqueSerialNo") - o2.getInt("uniqueSerialNo");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        OrderedMasterFeaturesList.forEach(m -> {
            masterFeaturesMergeMap.put(m.getString("featureCode"), m.getBoolean("merged"));
        });

        masterMarkets.forEach(mastreMarket -> {
            markets.put(mastreMarket.getString("market"), mastreMarket.getString("mid"));
        });

        masterManufacturers.forEach(manufacturer -> {
            manufacturers.put(manufacturer.getString("manufacturer"), manufacturer.getString("mfid"));
        });

        masterModels.forEach(model -> {
            models.put(model.getString("model"), model.getString("moid"));
        });
    }

    public void importSheet(final String filePath) {
        try {
            processFile(filePath);
        } catch (Exception e) {
            e.printStackTrace();

            //TODO: Send an error email indicating that the file could not be uploaded
        }
    }

    public List<String> readContent(File file) throws IOException {
        List<String> files = new ArrayList<String>();
        if (!file.getCanonicalPath().contains("Archive") && !file.getCanonicalPath().contains("archive")
                && !file.getCanonicalPath().contains("Archives") && !file.getCanonicalPath().contains("archives")
                && !file.getCanonicalPath().contains("archeives") && !file.getCanonicalPath().contains("Archeives")
                &&!file.getCanonicalPath().contains("archieves") && !file.getCanonicalPath().contains("Archieves")
                &&!file.getCanonicalPath().contains("archieve") && !file.getCanonicalPath().contains("Archieve")
                && !file.getCanonicalPath().contains(".DS_Store"))
            files.add(file.getCanonicalPath());
        return files;
    }
    static JsonObject jsn1 = new JsonObject();

    @SuppressWarnings("unchecked")
    public void processFile(String filePath) throws Exception {
        try {

            JsonObject carsJson = new JsonObject();
            int i = 7;

            FileInputStream excelFile = new FileInputStream(new File(filePath));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);

            Row row1 = datatypeSheet.getRow(datatypeSheet.getFirstRowNum() + 1);

            if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
                datatypeSheet = workbook.getSheetAt(1);
                row1 = datatypeSheet.getRow(datatypeSheet.getFirstRowNum() + 1);
                if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
                    datatypeSheet = workbook.getSheetAt(2);
                    row1 = datatypeSheet.getRow(datatypeSheet.getFirstRowNum() + 1);
                }
            }

            int lastRowTocheck = datatypeSheet.getLastRowNum();
            if (lastRowTocheck > 579) {
                lastRowTocheck = 579;
            } else {
                lastRowTocheck = datatypeSheet.getLastRowNum();
            }

            outer: while (i < datatypeSheet.getRow(4).getLastCellNum() && datatypeSheet.getRow(4).getCell(i) != null) {
                RowCount c = new RowCount();

                if(datatypeSheet.getRow(c.getRowCount()).getCell(1) != null) {
                    inner: while (c.getRowCount() <= lastRowTocheck && !(String.valueOf(datatypeSheet.getRow(c.getRowCount()).getCell(1)).equals(""))) {
                        JsonObject jsn = new JsonObject();
                        Row row = datatypeSheet.getRow(c.getRowCount());

                        final String featureCode = row != null ? row.getCell(1) != null && !(row.getCell(1).toString().equals(""))
                                ? Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString()))): "": "";
                        if (!masterFeaturesMergeMap.containsKey(featureCode)) {
                            c.incrementRowCount();
                            continue inner;
                        }
                        final String category = getCategory(row, i);
                        String cellComment = "";
                        if (masterFeaturesMergeMap.get(featureCode).booleanValue() == false) {
                            final String price = row.getCell(i + 1) != null ? validateValue(row, row.getCell(i + 1).toString().trim()): "";
                            final String comment = row.getCell(i + 2) != null ? row.getCell(i + 2).toString().trim() : "";

                            try {
                                cellComment = row.getCell(i) != null ? row.getCell(i).getCellComment() != null ? row.getCell(i).getCellComment().getString().toString() + "".trim(): "": ""
                                        //+ row.getCell(i + 1) != null ? row.getCell(i + 1).getCellComment() != null ? row.getCell(i + 1).getCellComment().getString().toString()+ "".trim(): "": ""
                                        + row.getCell(i + 1) != null && !String.valueOf(row.getCell(i+1)).equals("")? row.getCell(i + 1).getCellComment() != null && !String.valueOf(row.getCell(i + 1).getCellComment()).equals("")? row.getCell(i + 1).getCellComment().getString().toString()+ "".trim(): "": ""
                                        + row.getCell(i + 2) != null ? row.getCell(i + 2).getCellComment() != null ? row.getCell(i + 2).getCellComment().getString().toString() + "".trim(): "" : "";

                            } catch(NullPointerException e) {
//                                fileList.add(row.getRowNum()+" " + i +filePath);
                            }

                            jsn.addProperty("price", price);
                            jsn.addProperty("comment", (comment.concat(" " + cellComment)).trim());

                        } else {
                            jsn.addProperty("price", "");
                            cellComment = row.getCell(i) != null ? row.getCell(i).getCellComment() != null ? String.valueOf(row.getCell(i).getCellComment().getString() + "").trim(): "" : "";
                            jsn.addProperty("comment", cellComment.trim());
                        }

                        jsn.addProperty("category", category);
                        jsn1.add(featureCode, jsn);

                        c.incrementRowCount();
                    }
                }

                if (!markets.containsKey(jsn1.get("1").getAsJsonObject().get("category").getAsString())
                        || !manufacturers.containsKey(jsn1.get("2").getAsJsonObject().get("category").getAsString())
                        || !models.containsKey(jsn1.get("3").getAsJsonObject().get("category").getAsString())) {
                    i = i + 3;
                    carsJson = new JsonObject();
                    continue outer;
                }
                AppValidatorOpt appValidator = new AppValidatorOpt();
                String errorString = appValidator.validateCategoryRules(filePath, "", datatypeSheet, lastRowTocheck, i);
                if(!errorString.equals("")) {
                    i = i + 3;
                    carsJson = new JsonObject();
                    continue outer;
                }
                if (jsn1.get("2").getAsJsonObject().get("category").getAsString().equalsIgnoreCase("Jaguar") || jsn1
                        .get("2").getAsJsonObject().get("category").getAsString().equalsIgnoreCase("Land Rover")) {
                    i = i + 3;
                    carsJson = new JsonObject();
                    continue outer;
                }

                String mid = markets.get(jsn1.get("1").getAsJsonObject().get("category").getAsString());
                String market = jsn1.get("1").getAsJsonObject().get("category").getAsString();
                String mfid = manufacturers.get(jsn1.get("2").getAsJsonObject().get("category").getAsString());
                String manufacturer = jsn1.get("2").getAsJsonObject().get("category").getAsString();
                String moid = models.get(jsn1.get("3").getAsJsonObject().get("category").getAsString());
                String model = jsn1.get("3").getAsJsonObject().get("category").getAsString();
                String trim = jsn1.get("4").getAsJsonObject().get("category").getAsString();
                String modelYear = jsn1.get("5").getAsJsonObject().get("category").getAsString();
                String fuel = jsn1.get("8").getAsJsonObject().get("category").getAsString();
                String price = validatePrice(jsn1.get("10").getAsJsonObject().get("category").getAsString());
                String modelCode = jsn1.get("9").getAsJsonObject().get("category").getAsString();
                String power = jsn1.get("7").getAsJsonObject().get("category").getAsString();
                String engineCapacity = validatePrice(jsn1.get("6").getAsJsonObject().get("category").getAsString());
                String bodyType = jsn1.get("580").getAsJsonObject().get("category").getAsString();
                String version = jsn1.get("583").getAsJsonObject().get("category").getAsString();
                String specDate = jsn1.get("13").getAsJsonObject().get("category").getAsString();

                carsJson.addProperty("mid", mid);
                carsJson.addProperty("market", market);
                carsJson.addProperty("mfid", mfid);
                carsJson.addProperty("manufacturer", manufacturer);
                carsJson.addProperty("moid", moid);
                carsJson.addProperty("model", model);
                carsJson.addProperty("trim", trim);
                carsJson.addProperty("modelYear", modelYear);
                carsJson.addProperty("fuel", fuel);
                carsJson.addProperty("price", price);
                carsJson.addProperty("modelCode", modelCode);
                carsJson.addProperty("power", power);
                carsJson.addProperty("engineCapacity", engineCapacity);
                carsJson.addProperty("bodyType", bodyType);
                carsJson.addProperty("version", version);
                carsJson.addProperty("spec_date", specDate);

                carsJson.add("features", jsn1);
                carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));

                if (carsJson.get("model") != null) {
                    List<JsonObject> list = Db.searchAsJson(Query.select("type").from("CarTypes")
                            .where(SearchParam.create("model").eq(carsJson.get("model").toString().trim())));
                    if (list.size() > 0) {
                        carsJson.add("segment", list.get(0).get("type"));
                    } else
                        carsJson.addProperty("segment", "");
                }

                String jsonCarObjectName = market + manufacturer + model + trim + modelYear + fuel + power + specDate;
                CarModelsCacheData cacheData = new CarModelsCacheData(market, manufacturer, model, trim, modelYear,
                        fuel, power, specDate);

                if (cacheData.containsModel(jsonCarObjectName)) {
                    JSONObject obj = cacheData.getModel(jsonCarObjectName);
                    String cid = (String) obj.remove("cid");
                    obj.remove("parCode");
                    String _id = (String) obj.remove("_id");
                    String dbFilename = (String) obj.remove("fileName");
                    JsonElement currentFileName = carsJson.remove("fileName");
                    String filename = currentFileName.toString();

                    if (!JSON.areEqual(carsJson.toString(), obj.toString())) {
                        if (carsJson.get("spec_date").getAsString().equals(obj.get("spec_date").toString())) {

                            duplicateRecords.add(carsJson.get("market").getAsString() + "  "
                                    + carsJson.get("manufacturer").getAsString() + "  "
                                    + carsJson.get("model").getAsString() + "  " + carsJson.get("trim").getAsString()
                                    + "  " + carsJson.get("modelYear").getAsString() + "  "
                                    + carsJson.get("fuel").getAsString() + "  " + carsJson.get("power").getAsString()
                                    + "   " + filePath + "   " + dbFilename);
                            Db.execute("delete from `jlr-db-sb`.`CarModels` where `_id`= '" + _id + "'");
                            carsJson.addProperty("cid", cid);
                        } else {
                            carsJson.addProperty("cid", UUID.randomUUID().toString());
                        }

                        carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
                        Db.insertJson("CarModels", carsJson);
                    }
                } else {
                    carsJson.addProperty("fileName", filePath.substring(filePath.lastIndexOf("/") + 1));
                    carsJson.addProperty("cid", UUID.randomUUID().toString());
                    Db.insertJson("CarModels", carsJson);
                }
                carsJson = new JsonObject();
                i = i + 3;
            }
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getCategory(Row row, int i) {
        if (row.getCell(i) != null) {
            switch (row.getCell(i).toString()) {

                case "E-pace":
                    return "E-PACE";

                case "E-Pace":
                    return "E-PACE";

                case "I-Pace":
                    return "I-PACE";

                case "F-Pace":
                    return "F-PACE";

                case "5 series":
                    return "5 Series";

                case "S-Class":
                    return "S Class";

                case "Land Cruiser":
                    return "Land Cruiser Prado";

                case "911.0":
                    return "911";

                case "EU6":
                    return "Euro 6";

                default:
                    return row.getCell(i) != null ? validateValue(row, row.getCell(i).toString().trim()) : "";
            }
        }
        return row.getCell(i) != null ? validateValue(row, row.getCell(i).toString().trim()) : "";
    }

    public String validateValue(Row row, String val) {
        try {
            if (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 502) {
                if(val.equals("S")) {
                    Map<String, String> feaureCodeList = new HashMap<>();
                    feaureCodeList.put("497", "S");
                    feaureCodeList.put("498", "S");
                    feaureCodeList.put("499", "S");
                    feaureCodeList.put("503", "NA");

                    updateCategory(feaureCodeList);
                }
                return String.valueOf(val).trim();
            } else if (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 503) {
                if(val.equals("S")) {
                    Map<String, String> feaureCodeList = new HashMap<>();
                    feaureCodeList.put("497", "S");
                    feaureCodeList.put("498", "S");
                    feaureCodeList.put("499", "S");
                    feaureCodeList.put("502", "NA");

                    updateCategory(feaureCodeList);
                }
                return String.valueOf(val).trim();
            } else if (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 499) {
                if(val.equals("S")) {
                    Map<String, String> feaureCodeList = new HashMap<>();
                    feaureCodeList.put("497", "S");
                    feaureCodeList.put("498", "S");

                    updateCategory(feaureCodeList);
                }
                return String.valueOf(val).trim();
            }
            Double dValue = Double.parseDouble(val);
            if (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 14
                    || Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 16
                    || Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 4) {
                return String.valueOf(val).trim();
            } else if (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString())))) == 5) {
                return dValue != null ? Long.toString(Math.round(dValue)).trim().substring(2, 4) + "MY" : "";
            } else
                return dValue != null ? Long.toString(Math.round(dValue)).trim() : "";

        } catch (NumberFormatException e) {
        }
        return String.valueOf(val).trim();
    }

    private void updateCategory(Map<String, String> feaureCodeList) {
        feaureCodeList.forEach((featureCode, category)->{
            JsonObject jsn = new JsonObject();
            jsn.addProperty("price", jsn1.has(featureCode) ? jsn1.get(featureCode).getAsJsonObject().get("price").getAsString() :"");
            jsn.addProperty("comment", jsn1.has(featureCode) ? jsn1.get(featureCode).getAsJsonObject().get("comment").getAsString():"");
            jsn.addProperty("category", category);
            jsn1.add(featureCode, jsn);
        });
    }

    public String validatePrice(String val) {
        try {
            Double dValue = Double.parseDouble(val);
            return dValue != null ? Long.toString(Math.round(dValue)).trim() : "";
        } catch (NumberFormatException e) {
        }
        return String.valueOf(val).trim();
    }
}

class RowCount {
    int row = 1;

    public void incrementRowCount() {
        this.row += 1;
    }

    public int getRowCount() {
        return this.row;
    }
}
